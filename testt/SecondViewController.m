//
//  SecondViewController.m
//  testt
//
//  Created by fs on 2016/08/14.
//  Copyright © 2016年 fs. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"viewDidLoad%@",self.pushWords);
    // Do any additional setup after loading the view.
}


- (void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:YES];
     NSLog(@"viewDidAppear%@",self.pushWords);
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
     NSLog(@"viewWillAppear%@",self.pushWords);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
