//
//  AppDelegate.h
//  testt
//
//  Created by fs on 2016/07/25.
//  Copyright (c) 2016年 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
