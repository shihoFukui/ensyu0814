//
//  ViewController.m
//  testt
//
//  Created by fs on 2016/07/25.
//  Copyright (c) 2016年 ___FULLUSERNAME___. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"


@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{

    
    //遷移後の画面を取得する
    SecondViewController *secondView = (SecondViewController *)segue.destinationViewController;
    secondView.pushWords = @"第一画面";
    
}

@end
